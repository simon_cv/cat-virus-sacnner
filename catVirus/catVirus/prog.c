/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:Simon                     *
* Credits:                       *
**********************************/

#include <stdio.h>
#include "dirent.h"
#include <string.h>
#include <stdlib.h>
#define STR_LEN 300
#define BUFFER_LEN 1024
#define NUM_OF_PARAM_NEEDED 2
#define DIR_PATH 1
#define SIGNATURE_PATH 2
#define TWENTY_PERCENTS 0.2
#define EIGHTY_PERCENTS 0.8
#define TRUE 1
#define NULL_BYTE 1

void writeStartInLog(FILE* logFile, char** argv, int choice);
void checkDir(DIR* dirToScan, FILE* virusSignature, FILE* logFiles, char** argv, int choice);
int checkFile(FILE* fileToCheck, FILE* virusContent);
char* getVirusContent(FILE* virusSignature);
int findSize(FILE* file);
void startScan(int* choice, char** argv);
int checkTwentyPercents(FILE* fileToCheck, FILE* virusSignature, int* last, int* first, int* iteration);
char** makeArrayOfPaths(struct dirent* dir, DIR* dirToScan);
void sortArray(char** arrayOfPaths, int counter);
void swap(char* j, char* jp);
void writeArrayInLogAndPrint(char** arrayOfPaths, FILE* logFiles, int counter);
void freeArrOfPaths(char** arrayOfPaths, int counter);

int main(int argc, char** argv)
{
	int choice = 0;
	DIR* dirToScan = NULL;
	FILE* virusSignature = NULL;
	FILE* logFile = NULL;
	if (argc > NUM_OF_PARAM_NEEDED)//checking if enough parameters were entered
	{
		startScan(&choice, argv);
		dirToScan = opendir(argv[DIR_PATH]);//opening directory
		if (dirToScan != NULL)
		{
			virusSignature = fopen(argv[SIGNATURE_PATH], "rb");
			if (virusSignature != NULL)
			{
				logFile = fopen("logFile.txt", "w");
				if (logFile != NULL)
				{
					writeStartInLog(logFile, argv, choice);
					checkDir(dirToScan, virusSignature, logFile, argv, choice);
					printf("Scan Commplete!\n");
				}
				else
				{
					printf("Problen creating log file");
				}
			}
			else
			{
				printf("Problem opening Virus signature");
			}
		}
		else
		{
			printf("Problem opening Directory");
		}
	}
	else
	{
		printf("Usage: <Dir name to scan> <Virus signature file>");
	}
	getchar();
	return 0;
}

/*function that writes start in log file
input-
logFile - pointer to the log file
argv- array of pointers with name of files
choice - how to scan the directory
output-
none
*/
void writeStartInLog(FILE* logFile, char** argv, int choice)
{
	char varName[STR_LEN] = { 0 };
	fwrite("Anti-virus began! Welcome!\n\nFolder to scan:\n", sizeof(char), strlen("Anti - virus began!Welcome!\n\n	\n"), logFile);
	strcpy(varName, argv[DIR_PATH]);
	fwrite(varName, sizeof(char), strlen(varName), logFile);
	fwrite("\nVirus signature:\n", sizeof(char), strlen("\nVirus signature:\n"), logFile);
	strcpy(varName, argv[SIGNATURE_PATH]);
	fwrite(varName, sizeof(char), strlen(varName), logFile);
	if (!choice)
	{
		fwrite("\nScanning options:\nNormal\n", sizeof(char), strlen("\nScanning options:\nNormal\n"), logFile);
	}
	else
	{
		fwrite("\nScanning options:\nQuick Scan\n", sizeof(char), strlen("\nScanning options:\nQuick Scan\n"), logFile);
	}
	fwrite("Results:\n", sizeof(char), strlen("Results:\n"), logFile);
}

/*function that checks the files in the directory
input-
dirToScan - pointer to the directory to scan
virusSignature - the signature to check in the files
logFiles - pointer to the log files
argv - array of pointers with name of files
output-
none
*/
void checkDir(DIR* dirToScan, FILE* virusSignature, FILE* logFiles, char** argv, int choice)
{
	int twentyPercents = 0;
	int virusOrNot = 0;
	FILE* fileToCheck = NULL;
	char newPath[STR_LEN] = { 0 };
	struct dirent* dir = NULL;
	int first = 0;
	int last = 0;
	int iteration = 0;
	char** arrayOfPaths = NULL;
	int counter = 0;
	char* newPathCopy = NULL;
	arrayOfPaths = makeArrayOfPaths(dir, dirToScan);
	dirToScan = opendir(argv[DIR_PATH]);//opening directory
	if (dirToScan != NULL)
	{
		while ((dir = readdir(dirToScan)) != NULL)
		{
			if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") && dir->d_type != DT_DIR)
			{
				strcpy(newPath, argv[1]);
				strncat(newPath, "\\", strlen("\\"));
				strncat(newPath, dir->d_name, strlen(dir->d_name));
				fileToCheck = fopen(newPath, "rb");
				if (!choice)
				{
					virusOrNot = checkFile(fileToCheck, virusSignature);
				}
				else
				{
					iteration = 0;
					fseek(fileToCheck, 0, SEEK_SET);
					virusOrNot = checkTwentyPercents(fileToCheck, virusSignature, &last, &first, &iteration);//checking first 20%
					if (!virusOrNot)//checking last 20%
					{
						virusOrNot = checkTwentyPercents(fileToCheck, virusSignature, &last, &first, &iteration);
					}
					if (!virusOrNot)//if virus not found in the first or last 20%, scanning the whole file for the virus
					{
						fseek(fileToCheck, 0, SEEK_SET);
						virusOrNot = checkFile(fileToCheck, virusSignature);
					}
				}
				if (!first && !last)
				{
					if (virusOrNot)
					{
						strncat(newPath, " - Infected!\n", strlen(" - Infected!\n"));
					}
					else
					{
						strncat(newPath," - Clean\n", strlen(" - Clean\n"));
					}
				}
				else
				{
					if (first)
					{
						strncat(newPath, " - Infected! (first 20%)\n", strlen(" - Infected! (first 20%)\n"));
					}
					else
					{
						strncat(newPath, " - Infected! (last 20%)\n", strlen(" - Infected! (last 20%)\n"));
					}
				}
				newPathCopy = (char*)malloc(sizeof(char) * (strlen(newPath)+NULL_BYTE));
				strcpy(newPathCopy, newPath);
				arrayOfPaths[counter] = newPathCopy;
				counter += 1;
			}
		}
		closedir(dirToScan);
		sortArray(arrayOfPaths, counter);
		writeArrayInLogAndPrint(arrayOfPaths, logFiles, counter);
		freeArrOfPaths(arrayOfPaths, counter);
		fclose(logFiles);
		fclose(fileToCheck);
		fclose(virusSignature);
	}
	else
	{
		printf("Problem opening directory");
	}
}

/*function that checks the file for the virus signature
input-
virusSignature - the signature to check in the files
fileToCheck - pointer to the file to check
output-
VirusOrNot - is the file is infected or not
*/
int checkFile(FILE* fileToCheck, FILE* virusSignature)
{
	int virusOrNot = 0;
	int size = 0;
	char buffer[BUFFER_LEN] = { 0 };
	char* virusContent = NULL;
	int i = 0;
	int j = 0;
	int VirusSignatureLen = findSize(virusSignature);
	virusContent = getVirusContent(virusSignature);
	fseek(virusSignature, 0, SEEK_SET);
	while (((size = fread(buffer, sizeof(char), BUFFER_LEN, fileToCheck)) != 0) && !virusOrNot)
	{

		for (i = 0; i < size && !virusOrNot; i++)
		{
			if (buffer[i] == virusContent[j])
			{
				j += 1;
				if (j + 1 == VirusSignatureLen)
				{
					virusOrNot = TRUE;
				}
			}
			else
			{
				j = 0;
			}

		}
	}
	free(virusContent);
	return virusOrNot;
}


/*function that gets file size
input:
file - the file to get its size
output:
size of file
*/
int findSize(FILE* file)
{
	int size = 0;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	return size;
}

/*function that gets virus content
input:
virusSignature - the file to get its content
output:
pointer to array that hold content of virus
*/
char* getVirusContent(FILE* virusSignature)
{
	char* buffer = (char*)malloc(sizeof(char) * findSize(virusSignature));
	fread(buffer, sizeof(char), findSize(virusSignature), virusSignature);
	return buffer;
}

/*function that prints start and lets user to pick scan choice
input:
choice - address of choice
argv- array of pointer that includes paths
output:
none
*/
void startScan(int* choice, char** argv)
{
	printf("Welcome to my Virus scan!\n\nFolder to scan- %s\nVirus signature- %s\n\nPress 0 for a normal scan or any other key for a quick scan:  ", argv[1], argv[2]);
	scanf("%d", choice);
	printf("Scanning began...\nThis process may take several minute...\nScanning:\n");
}

/*function that checks the file for the virus signature if fast scan was chosen
input-
virusSignature - the signature to check in the files
fileToCheck - pointer to the file to check
last - pointer to flag to check if virus was found in first or last 20 percents
first - pointer to flag to check if virus was found in first or last 20 percents
iteration - pointer to int that counts the run of the function to know which flag to activate
output-
virusOrNot - is the file is infected or not
*/
int checkTwentyPercents(FILE* fileToCheck, FILE* virusSignature, int* last, int* first, int* iteration)
{
	char* buffer = NULL;
	int virusOrNot = 0;
	int i = 0;
	int j = 0;
	int size = 0;
	char* virusContent = NULL;
	int fileLen = 0;
	int virusSignatureLen = findSize(virusSignature);
	*last = 0;//resetting values for each scan
	*first = 0;//resetting values for each scan
	virusContent = getVirusContent(virusSignature);
	fseek(virusSignature, 0, SEEK_SET);
	fileLen = findSize(fileToCheck);
	fseek(fileToCheck, 0, SEEK_SET);
	buffer = (char*)malloc(sizeof(char) * (fileLen * TWENTY_PERCENTS));
	*iteration += 1;
	if (*iteration == 2)//checking if reading the last 20%.
	{
		fseek(fileToCheck, (fileLen * EIGHTY_PERCENTS), SEEK_SET);
	}
	size = fread(buffer, sizeof(char), (fileLen * TWENTY_PERCENTS), fileToCheck);

	for (i = 0; i < size && !virusOrNot; i++)
	{
		if (buffer[i] == virusContent[j])
		{
			j += 1;
			if (j + 1 == virusSignatureLen)
			{
				virusOrNot = TRUE;
				if (*iteration == TRUE)
				{
					*first = TRUE;
				}
				else
				{
					*last = TRUE;
				}
			}
		}
		else
		{
			j = 0;
		}

	}
	free(buffer);
	free(virusContent);
	return virusOrNot;
}

/*function that checks the num of files in the dir and makes and array of pointers according to that.
input-
dir - dirent struct
DirToScan- pointer to the dir to scan
output-
arr - The address of the array of pointers
*/
char** makeArrayOfPaths(struct dirent* dir, DIR* dirToScan)
{
	int counter = 0;
	char** arr = NULL;
	while ((dir = readdir(dirToScan)) != NULL)//counting the number of files in the dir 
	{
		if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") && dir->d_type != DT_DIR)
		{
			counter += 1;
		}

	}
	arr = (char**)malloc(sizeof(char*) * counter);
	closedir(dirToScan);
	return arr;
}

/*function that sorts the array in order
input-
arrayOfPaths - the array that contains the paths 
counter - num of cells in array
output-
none
*/
void sortArray(char** arrayOfPaths, int counter)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < counter - 1; i++)
	{
		for (j = 0; j < counter - i - 1; j++)
		{
			if (!strcmp(arrayOfPaths[j],arrayOfPaths[j + 1]))
			{
				swap(arrayOfPaths[j], arrayOfPaths[j + 1]);
			}
		}
	}

}

/*function that swaps cells in array
input-
j - address of j
jp - address of j+1
output-
none*/
void swap(char* j, char* jp)
{
	char* temp = jp;
	jp = j;
	j = temp;
}

/*function that writes paths in logfile and prints the results
input-
arrayOfPaths - the array that contains the paths
logFiles - pointer to the log file
counter - the number of cells in the array of paths
output-
none
*/
void writeArrayInLogAndPrint(char** arrayOfPaths, FILE* logFiles,int counter)
{
	int i = 0;
	for (i = 0; i < counter ; i++)
	{
		printf("%s\n", arrayOfPaths[i]);
		fwrite(arrayOfPaths[i], sizeof(char), strlen(arrayOfPaths[i]), logFiles);
		fwrite("\n", sizeof(char), strlen("\n"), logFiles);
	}
}

/*function that free array of paths
input-
arrayOfPaths - array needed to be realeased
counter - num of cells in array
output-
none
*/
void freeArrOfPaths(char** arrayOfPaths, int counter)
{
	int i = 0;
	for (i = 0; i < counter; i++)
	{
		free(arrayOfPaths[i]);
	}
	free(arrayOfPaths);
}